<?php

function chmod_r($path) {
    $dir = new DirectoryIterator($path);
    foreach ($dir as $item) {
        chmod($item->getPathname(), 0777);
        if ($item->isDir() && !$item->isDot()) {
            chmod_r($item->getPathname());
        }
    }
}


chmod_r('/opt/lampstack-7.3.19-0/apache2/htdocs/web/php/ebayEcom/public/items');



?>