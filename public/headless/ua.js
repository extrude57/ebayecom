"use strict";


// get the user agent with http://www.httpuseragent.org 
var page = require('webpage').create();
console.log('The default user agent is ' + page.settings.userAgent);
page.settings.userAgent = 'SpecialAgent';
page.open('https://google.com', function (status) {
    if (status !== 'success') {
        console.log('Unable to access network');
    } else {
        var ua = page.evaluate(function () {
            return document.getElementById('qua').value;
        });
        console.log(ua);
    }
    phantom.exit();
});