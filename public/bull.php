<?php
require_once('vendor/autoload.php');
require( Symfony\Component\DomCrawler\Crawler);

$html = <<<'HTML'
<!DOCTYPE html>
<html>
    <body>
        <p class="message">Hello World!</p>
        <p>Hello Crawler!</p>
    </body>
</html>
HTML;

$crawler = new Crawler($html);

foreach ($crawler as $domElement) {
    echo $domElement->nodeName;
}

?>