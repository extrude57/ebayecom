<?php
ob_start();
session_start();
// For debugging purposes
//session_destroy();
defined("DS") ? null : define("DS",DIRECTORY_SEPARATOR);
defined("TEMPLATE_FRONT") ? null : define("TEMPLATE_FRONT",__DIR__.DS."template/front");
defined("TEMPLATE_BACK") ? null : define("TEMPLATE_BACK",__DIR__.DS."template/back");
#defined("DS_ITEMS") ? null : define("DS_ITEMS",__DIR__.DS."../public/items/mens/hats/jaxon/ivy_summer_fun_cap");

defined("DB_HOST") ? null : define("DB_HOST","localhost");
defined("DB_USER") ? null : define("DB_USER","root");
defined("DB_PASS") ? null : define("DB_PASS","");
defined("DB_NAME") ? null : define("DB_NAME","ecom_db");
$connection =mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
require_once("functions.php");
/*
** Magic Constants
*/
//echo __DIR__ ."<br>"; // This gives the path of server's directory
//echo __FILE__ ."<br>";  // This gives the path of the file's directory & the file name
//echo TEMPLATE_FRONT."<br>";
//echo TEMPLATE_BACK;
?>
